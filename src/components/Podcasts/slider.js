import React, { useState, useRef, useEffect } from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'
import Swiper from 'swiper'
import 'swiper/css/swiper.min.css'
import media from "../../style/media"
import { ParallaxBanner } from 'react-scroll-parallax'

import Pod1 from '../../images/Pod-images/pod1.jpg'
import Pod2 from '../../images/Pod-images/pod2.jpg'
import Pod3 from '../../images/Pod-images/pod3.jpg'
import Pod4 from '../../images/Pod-images/pod4.jpg'

const SwiperCon = styled.div`
  overflow: visible;
  ${media.laptopM`
    img {
      height: 500px;
    }
    .swiper-container {
      overflow: visible;
      .swiper-wrapper {
      overflow: visible;
        .swiper-slide {
          padding: 2em 0;
        }
        .swiper-slide-duplicate-active,
        .swiper-slide-active {
          &:hover {
            transform: scale(1.05);
            transition-duration: 0.5s;
          }
          transform-origin: center center;
          transform: rotate(0deg);
          transition-duration: 0.5s;
        }
        .swiper-slide-duplicate-prev,
        .swiper-slide-prev {
          transform-origin: center center;
          /* transform: rotate(-15deg); */
          transform: scale(0.5);
          transition-duration: 2s;
        }
        .swiper-slide-duplicate-next,
        .swiper-slide-next {
          transform-origin: center center;
          transform: rotate(15deg);
          transform: scale(0.9);
          transition-duration: 2s;
        }
      }
    }
  `}
  ${media.laptopL`
    img {
      height: 550px;
    }
    .swiper-container {
      .swiper-wrapper {
        .swiper-slide {
          padding: 4em 0;
        }
      }
    }
  `}
`

const PodcastSlider = () => {
  const swiperElement = useRef(null)
  const swiper = useRef(null)

  const [activeSlide, setActiveSlide] = useState(0)

  useEffect(() => {
    swiper.current = new Swiper(swiperElement.current, {
      centeredSlides: true,
      paginationClickable: true,
      loop: true,
      mousewheel: true,
      slidesPerView: 2,
      spaceBetween: 400,
      breakpoints: {
        1600: {       
          slidesPerView: 2.2,
          spaceBetween: 500,
        }
      },
      speed: 500,
      grabCursor: true,
      parallax: true,
    })
  }, [])

  return (
    <SwiperCon>
      <div ref={swiperElement} className="swiper-container">
        <div className="swiper-wrapper">
          <div className="swiper-slide">
            <img src={Pod1} /> 
          </div>
          <div className="swiper-slide">
            <img src={Pod2} /> 
          </div>
          <div className="swiper-slide">
            <img src={Pod3} /> 
          </div>
          <div className="swiper-slide">
            <img src={Pod4} /> 
          </div>
        </div>
      </div>
    </SwiperCon>
  )
}

export default PodcastSlider
