import React, { useState, useRef, useEffect, Fragment } from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'
import Media from 'react-media'
import media from "../../style/media"
import { ParallaxProvider } from 'react-scroll-parallax'

import PodcastSlider from './slider.js'
import PodcastList from './list.js'

const PodcastCon = styled.div`
  ${media.laptopM`
    min-height: calc(100vh - 14em);
    img {
      height: 500px;
    }
    .swiper-container {
      .swiper-wrapper {
        .swiper-slide {
          padding: 2em 0;
        }
        .swiper-slide-duplicate-active,
        .swiper-slide-active {
          transform-origin: center center;
          transform: rotate(0deg);
          transition-duration: 2s;
        }
        .swiper-slide-duplicate-prev,
        .swiper-slide-prev {
          transform-origin: center center;
          transform: rotate(-15deg);
          transition-duration: 2s;
        }
        .swiper-slide-duplicate-next,
        .swiper-slide-next {
          transform-origin: center center;
          transform: rotate(15deg);
          transition-duration: 2s;
        }
      }
    }
  `}
  ${media.laptopL`
    img {
      height: 550px;
    }
    .swiper-container {
      .swiper-wrapper {
        .swiper-slide {
          padding: 4em 0;
        }
      }
    }
  `}
`

const Podcasts = () => {

  return (
    <ParallaxProvider>
      <PodcastCon>
        <Media queries={{
          mobile: "(max-width: 599px)",
          laptop: "(min-width: 800px)"
        }}>
          {matches => (
            <Fragment>
              {matches.mobile && <PodcastList />}
              {matches.laptop && <PodcastSlider />}
            </Fragment>
          )}
        </Media>
      </PodcastCon>
    </ParallaxProvider>
  )
}

export default Podcasts
