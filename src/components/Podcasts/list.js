import React, { useState, useRef, useEffect } from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'
import media from "../../style/media"
import { Parallax } from 'react-scroll-parallax'

import Pod1 from '../../images/Pod-images/pod1.jpg'
import Pod2 from '../../images/Pod-images/pod2.jpg'
import Pod3 from '../../images/Pod-images/pod3.jpg'
import Pod4 from '../../images/Pod-images/pod4.jpg'

const ListCon = styled.div`
  margin: 1em 2em;
  figure {
    margin: 0;
  }
  img {
    margin: 1.5em 0;
    width: 100%;
    position: relative;
  }
  p {
    /* margin: 1em 0.5em; */
    font-weight: 100;
    font-size: 2.5em;
  }
`

const PodcastList = () => {
  
  return (
    <ListCon>
      <p>a <b>podcast company</b> made under the <br/> African sky</p>
      <br/>
      <Parallax className="custom-class" y={[-20, 0]} tagOuter="figure">
        <div>
          <img src={Pod1} /> 
        </div>
      </Parallax>
      <Parallax className="custom-class" y={[-20, 0]} tagOuter="figure">
        <div>
          <img src={Pod2} /> 
        </div>
      </Parallax>
      <Parallax className="custom-class" y={[-20, 0]} tagOuter="figure">
        <div>
          <img src={Pod3} /> 
        </div>
      </Parallax>
      <Parallax className="custom-class" y={[-20, 0]} tagOuter="figure">
        <div>
          <img src={Pod4} /> 
        </div>
      </Parallax>
    </ListCon>
  )
}

export default PodcastList
