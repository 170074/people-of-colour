import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { Fragment } from "react"
import styled, { keyframes } from 'styled-components'
import media from "../../style/media"
import Media from 'react-media';

import Logo from '../../images/icons/poc-name.svg'
import Menu from '../../images/icons/menu.svg'

const MenuHover = keyframes`
  50% {
    transform: translateX(-21px);
  }
  100% {
    transform: translateX(0);
  }
`

const HeaderCon = styled.div`
  height: 3em;
  width: auto;
  display: grid;
  grid-template-columns: 3fr 0.5fr;
  grid-template-rows: 1fr;
  padding: 1em 2em;
  .logo {
    grid-column: 1;
  }
  .menu {
    grid-column: 2;
  }
  align-items: center;
  img {
    width: 12em;
  }
  ${media.laptop`
  width: auto;
    height: 5em;
    padding: 1em 3em;
    grid-template-columns: 5fr 0.5fr;
    grid-template-rows: 1fr;
    .menu {
      display: flex;
      p {
        text-transform: uppercase;
        padding: 0 2em;
        font-weight: 550;
        &:hover {
          color: #fdc14f;
        }
      }
      svg {
        margin: auto 0;
        &:hover {
          .line {
            animation: 1.5s ease ${MenuHover};
          }
        }
      }
    }
  `}
`

const Header = ({ siteTitle }) => (
  <header>
    <HeaderCon>
      <img className="logo" src={Logo} />
      <div className="menu">
      <Media queries={{
        mobile: "(max-width: 599px)",
        laptop: "(min-width: 800px)"
      }}>
        {matches => (
          <Fragment>
            {matches.laptop && <p>menu</p>}
          </Fragment>
        )}
      </Media>
        <svg width="42" height="15" viewBox="0 0 42 15" fill="none" xlink="http://www.w3.org/2000/svg">
          <line className="line" x1="8" y1="7.5" x2="42" y2="7.5" stroke="white" stroke-width="3" stroke-linejoin="round"/>
          <line className="line" x1="21" y1="1.5" x2="42" y2="1.5" stroke="white" stroke-width="3" stroke-linejoin="round"/>
          <line className="line" y1="13.5" x2="42" y2="13.5" stroke="white" stroke-width="3"/>
        </svg>  
      </div>
    </HeaderCon>
  </header>
)

export default Header
