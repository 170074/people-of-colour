import React from 'react'
import styled, { keyframes } from 'styled-components'

const RightFootstep = keyframes`
  50% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`

const AnimationContainer = styled.div`
  width : 100vw; 
  height: 100vh;
  svg {
    transform: translate(-50%, -50%);
    line {
      animation: 2s ease infinite ${RightFootstep};
    }
  }
`

const WalkingLoader2 = () => (
    <AnimationContainer>
    {/* <Shape version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 585.3 819.1" space="preserve"> */}
      <svg width="42" height="15" viewBox="0 0 42 15" fill="none" xlink="http://www.w3.org/2000/svg">
        <line x1="8" y1="7.5" x2="42" y2="7.5" stroke="white" stroke-width="3" stroke-linejoin="round"/>
        <line x1="21" y1="1.5" x2="42" y2="1.5" stroke="white" stroke-width="3" stroke-linejoin="round"/>
        <line y1="13.5" x2="42" y2="13.5" stroke="white" stroke-width="3"/>
      </svg>    
    </AnimationContainer>
)

export default WalkingLoader2
