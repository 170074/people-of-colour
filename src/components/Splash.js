/* eslint-disable react/prop-types */
import React from "react"
import styled, { keyframes } from "styled-components"

import Logo from '../images/icons/poc-logo.svg'

const Pulse = keyframes`
  0% {
    transform: scale(0.8);
  }50% {
    transform: scale(1);
  }
  100% {
    transform: scale(0.8);
  }
`

const Container = styled.section`
  position: fixed;
  z-index: 999;
  width: 100vw;
  height: 100vh;
  background-color: black;
  opacity: 1;
  &.disable {
    transition: 500ms ease-out;
    opacity: 0;
    z-index: -10;
  }
  img {
    position: absolute;
    display: block;
    margin: auto;
    width: 20vw;
    left: 40%;
    top: 20%;
    animation: 2s ease-in-out infinite ${Pulse};
  }
`
const ProgressBar = styled.div`
  border-radius: 50px;
  height: 5px;
  width: 150px;
  position: absolute;
  display: block;
  left: 50%;
  top: 80%;
  transform: translate(-50%, -50%);
  div {
    border-radius: 50px;
    height: 100%;
    background-color: white;
    transition: width 0.5s ease;
    -webkit-animation: load 2.2s linear;
    -moz-animation: load 2.2s linear;
    -o-animation: load 2.2s linear;
    animation: load 2.2s linear;
  }
  @-webkit-keyframes load {
    from {
      width: 0%;
    }
    to {
      width: 100%;
    }
  }
  @-moz-keyframes load {
    from {
      width: 0%;
    }
    to {
      width: 100%;
    }
  }
  @-o-keyframes load {
    from {
      width: 0%;
    }
    to {
      width: 100%;
    }
  }
  @keyframes load {
    from {
      width: 0%;
    }
    to {
      width: 100%;
    }
  }
`

const Splash = props => {
  return (
    <Container className={props.active ? "disable" : null}>
      <img src={Logo} />
      <ProgressBar>
        <div className={props.active ? "disable" : null} />
      </ProgressBar>
    </Container>
  )
}

export default Splash
