import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { Fragment } from "react"
import styled, {keyframes} from 'styled-components'
import media from "../../style/media"
import Media from 'react-media';

import Logo from '../../images/icons/poc-logo.svg'
import Facebook from '../../images/icons/facebook.png'
import Insta from '../../images/icons/insta.png'
import Twitter from '../../images/icons/twitter.png'

const SocialHover = keyframes`
  50% {
    transform: scale(1.2);
  }
  100% {
    transform: scale(1);
  }
`

const Pulse = keyframes`
  0% {
    transform: scale(0.8);
  }50% {
    transform: scale(1);
  }
  100% {
    transform: scale(0.8);
  }
`

const FooterCon = styled.div`
  height: 3em;
  width: auto;
  display: grid;
  align-items: center;
  grid-template-columns: 3fr 2fr;
  grid-template-rows: 1fr;
  padding: 1em 2em;
  .logo {
    p {
      font-weight: bold;
      font-size: 1em;
      letter-spacing: 1.5px;
    }
  }
  .social {
    margin: 0 0 0 1em;
    display: flex;
    text-align: right;
    p {
      padding: 0 0 0 1.5em;
    }
  }
  ${media.laptop`
    margin-top: 0;
    height: 5em;
    width: auto;
    padding: 1em 3em;
    grid-template-columns: 4fr 0.5fr;
    grid-template-rows: 1fr;
    .logo, .social {
      display: inline-flex;
    }
    .logo {
      img {
        height: 2.5em;
        margin: auto 0.5em;
        animation: 2s ease-in-out infinite ${Pulse};
      }
      p {
        text-transform: uppercase;
        font-size: 1.2em;
      }
    }
    .social {
      img {
        height: 2em;
        margin: 0 0.5em;
        &:hover {
          animation: 1s ease ${SocialHover};
        }
      }
    }
  `}
`

const Footer = ({ siteTitle }) => (
  <FooterCon>
    <div className="logo">
    <Media queries={{
        mobile: "(max-width: 599px)",
        laptop: "(min-width: 800px)"
      }}>
        {matches => (
          <Fragment>
            {matches.laptop && <img src={Logo} />}
          </Fragment>
        )}
    </Media>
      <p>People of colour.</p>
    </div>
    <div className="social">
    <Media queries={{
        mobile: "(max-width: 599px)",
        laptop: "(min-width: 800px)"
      }}>
        {matches => (
          <Fragment>
            {matches.mobile && 
              <>
              <p>Tw</p>
              <p>Fb</p>
              <p>Ig</p>
              </>
            }
            {matches.laptop && 
              <>
              <img src={Twitter} />
              <img src={Facebook} />
              <img src={Insta} />
              </>
            }
          </Fragment>
        )}
    </Media>
    </div>
  </FooterCon>
)

export default Footer
