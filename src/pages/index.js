import React, { PureComponent } from "react"

import Layout from "../components/layout"
import Splash from "../components/Splash"
import SEO from "../components/seo"
import Podcasts from "../components/Podcasts"

class IndexPage extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      hasLoaded: false,
    }
  }

  componentDidMount() {
    this.imgLoaded()
  }

  imgLoaded = () => {
    let self = this
    var pageImgs = document.images,
      imageLength = pageImgs.length,
      counter = 0
    ;[].forEach.call(pageImgs, function(img) {
      if (img.complete) {
        incrementCounter()
      } else {
        img.addEventListener("load", incrementCounter, false)
      }
    })

    function incrementCounter() {
      counter++
      if (counter === imageLength) {
        self.setState({ hasLoaded: true })
      }
    }
  }

  render() {
    let { hasLoaded } = this.state

    return (
      <>
      <Splash active={hasLoaded} progress={this.imagesLoaded} />
      <Layout>
        <SEO title="Home" />
        <Podcasts />
      </Layout>
      </>
    )
  }
}

export default IndexPage
