## 🚀 Get started

1.  **Clone the repository.**

    using git clone

2.  **Add packages.**

    By running yarn install
    
2.  **Run the project.**

    using gatsby devlop

4.  **Incase of error**

    If you get an error that states: Something went wrong installing the "sharp" module,
    run npm rebuild --verbose sharp
